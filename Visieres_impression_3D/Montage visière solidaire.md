# Montage Visière solidaire - Covid19 - V1x

- ![visière solidaire](https://gitlab.com/entraide-maker-covid-19/visieres/-/raw/master/images/visieresolidaire.jpg)
  - Lien d'origine : https://www.thingiverse.com/thing:4238879

## Le matériel

* Pièces imprimées:
- ![Pièces imprimées](images/01-les_pieces.jpg)
* Visière (Feuille PVC A4):
- ![Pièces imprimées](images/02-feuille_pvc.jpg)
* Elastiques:
- ![Elastiques Classiques](https://www.toutembal.fr/client/gfx/photos/produit/bracelet_elastique_en_caoutchouc_1mm8_1155.jpg)
- ![Ruban Elastiques](https://static-mapetitemercerie.o10c.net/52234-large_default/elastique-plat-5-mm-rouge.jpg "Ruban élastique")



## Le Montage

* Placer la feuille en paysage dans l'une des encoches sur le coté:
- ![Montage Feuille](images/03-montage_feuille.jpg)

* Positionner le 1er clip  et l'enfoncer:
- ![Positionner Clip](images/04-insertion-clip.jpg)
- ![Enfoncer Clip](images/05-clip_enfonce.jpg)

* Faire le clip opposé puis le clip central;
- ![Clip Central](images/06-tous_les_clips.jpg)

* Montage des élastiques:
- ![Elastiques Classiques](images/07-Elastiques_classiques.jpg)
- ![Ruban Elastiques](images/08-Ruban_elastiques.jpg)


## Désinfection:
### Work in Progress


## Liens vers des fournisseurs de feuilles plastiques

* Bureau vallée ont des drives.
* Eventuellement Office dépot
* Amazon - PAVO - Feuille PVC A4 200 micron: https://www.amazon.fr/gp/product/B00JPCN25Y/ref=ppx_yo_dt_b_asin_title_o00_s00?ie=UTF8&psc=1